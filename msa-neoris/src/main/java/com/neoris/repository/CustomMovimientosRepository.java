package com.neoris.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import com.neoris.dto.Filtros;
import com.neoris.entitie.Movimiento;


@Repository
public class CustomMovimientosRepository {
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Atributos de la clase
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** Atributo para realizar operaciones de recuperacion y consulta de entidades **/
	@PersistenceContext protected EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Movimiento> getMovimientos(Filtros filtro) {
		Query result = entityManager.createNativeQuery("""
				SELECT m.id, m.fecha Fecha, c.nombre Cliente, a.numero_cuenta AS Numero_Cuenta, m.tipo_movimiento,
				a.saldo_inicial,c.estado Estado, m.valor , m.saldo
				FROM cliente c
				INNER JOIN cuenta a ON a.cliente_id = c.id
				INNER JOIN movimiento m ON m.numero_cuenta = a.numero_cuenta 
				WHERE c.id = :cliente AND (m.fecha BETWEEN :desde AND :hasta) 
				""", Movimiento.class);
		result.setParameter("desde", filtro.getDesde());
		result.setParameter("hasta", filtro.getHasta());	
		result.setParameter("cliente", filtro.getCliente().getId());		
		return result.getResultList();
	}
	
	
}

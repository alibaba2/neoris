package com.neoris.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neoris.entitie.Cliente;

/**
 * Clase generica para manejar las transacciones del cliente  
 * 
 * @author Edward Daniel Vidal García
 * @since diciembre 2022
 */
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {		

}

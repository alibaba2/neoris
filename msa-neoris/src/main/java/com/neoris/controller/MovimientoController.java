package com.neoris.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.neoris.dto.Filtros;
import com.neoris.entitie.Cliente;
import com.neoris.entitie.Movimiento;
import com.neoris.service.MovimientoService;

@RestController
public class MovimientoController {

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Atributos de la clase
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//** variable para el manejo del log */
	private static final Logger log = LoggerFactory.getLogger(ClienteController.class);
	
	/** Instancia del servicio para manejar los metodos del servicio */
	@Autowired
	private MovimientoService movimientoService;	
	
	@PostMapping("movimientos")
	public ResponseEntity<String> createCliente(@RequestBody Movimiento data, BindingResult error) {
		log.info("start insert data Movimiento");
		if(error.hasErrors()) {
			throw new IllegalArgumentException(error.toString());			
		}
		try {
			log.info("get data Movimiento");
			return ResponseEntity.ok(movimientoService.createMovimiento(data));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@PostMapping("reportes")
	public ResponseEntity<List<Movimiento>> reporteMovimientos(@RequestBody Filtros filtro, BindingResult error) {
		log.info("select data Movimiento");
		if(error.hasErrors()) {
			throw new IllegalArgumentException(error.toString());			
		}
		try {
			log.info("get data Movimiento");
			return ResponseEntity.ok(movimientoService.getMovimientos(filtro));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	
}

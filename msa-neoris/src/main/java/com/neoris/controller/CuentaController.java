package com.neoris.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.neoris.entitie.Cuenta;
import com.neoris.service.CuentaService;

@RestController
public class CuentaController {
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Atributos de la clase
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//** variable para el manejo del log */
	private static final Logger log = LoggerFactory.getLogger(ClienteController.class);
	
	/** Instancia del servicio para manejar los metodos del servicio */
	@Autowired
	private CuentaService CuentaService;	
	
	/**
	* Metodo para crear cliente
	* @param data recibe los datos
	* @param error
	* @return
	*/
	@PostMapping("cuentas")
	public ResponseEntity <List<Cuenta>> createCliente(@RequestBody List<Cuenta> data, BindingResult error) {
		log.info("start insert data cuenta");
		if(error.hasErrors()) {
			throw new IllegalArgumentException(error.toString());			
		}
		try {
			log.info("get data cuenta");
			return ResponseEntity.ok(CuentaService.createCuenta(data));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}	

}

package com.neoris.dto;

import java.time.LocalDate;

import com.neoris.entitie.Cliente;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase para capturar los filtros de la dispensacion
 * 
 * @author Edward Daniel Vidal García
 * @since Diciembre 2022
 */
@AllArgsConstructor
@Getter
@Setter
public class Filtros {
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Atributos de la clase
	///////////////////////////////////////////////////////////////////////////////////////////////////////////		
	
	/** filtro fecha desde*/
	private LocalDate desde;
	
	/** filtro fecha hasta*/
	private LocalDate hasta;	
	
	/** filtro cliente **/
	private Cliente cliente;
	
}

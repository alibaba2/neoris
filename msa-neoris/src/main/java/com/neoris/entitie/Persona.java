package com.neoris.entitie;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;

/**
 * Clase modelo para el manejo del persona 
 * 
 * @author Edward Daniel Vidal García
 * @since Diciembre 2022
 */ 
@MappedSuperclass
@Data
public class Persona {
	
	/** campo para guardar id de la entidad, autoincrementable */
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
		
	/** atributo para almacenar nombre */
	private String nombre;
	
	/** atributo para almacenar genero */
	private String genero;
	
	/** atributo para almacenar edad */
	private Integer edad;
	
	/** atributo para almacenar identificacion */
	private Integer identificacion;
	
	/** atributo para almacenar direccion */
	private String direccion;
	
	/** atributo para almacenar telefono */
	private String telefono;		
		

}

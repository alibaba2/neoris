package com.neoris.entitie;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * Clase modelo para el manejo del cliente 
 * 
 * @author Edward Daniel Vidal García
 * @since Diciembre 2022
 */
@Entity
@Table(name = "CLIENTE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@Data
public class Cliente extends Persona {						
	
	/** atributo para almacenar contrasena */
	@Column(name = "CONTRASENA")
	private String contrasena;
	
	/** atributo para almacenar estado */
	@Column(name = "ESTADO")
	private Boolean estado;


}

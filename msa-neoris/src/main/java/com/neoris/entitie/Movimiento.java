package com.neoris.entitie;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * Clase modelo para el manejo del movimiento 
 * 
 * @author Edward Daniel Vidal García
 * @since Diciembre 2022
 */
@Entity
@Table(name = "MOVIMIENTO")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@Data
public class Movimiento {
	
	/** campo para guardar id de la entidad, autoincrementable */
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;	

	/** Campo para guardar fecha */
	@Column(name = "fecha")
	private LocalDate fecha;
	
	/** Campo para guardar tipo movimiento*/
	@Column(name = "TIPO_MOVIMIENTO")
	private String tipoMovimiento;
	
	/** Campo para guardar valor*/
	@Column(name = "VALOR")
	private Long valor;
	
	/** Campo para guardar saldo*/
	@Column(name = "SALDO")
	private Long saldo;
	
	/** Campo para guardar bodega del dispensacion valor(numerico) */
	@ManyToOne(optional = true ,fetch = FetchType.LAZY)
	@JoinColumn(name="NUMERO_CUENTA")
	private Cuenta cuenta;

}

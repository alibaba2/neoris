package com.neoris.entitie;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * Clase modelo para el manejo del cuenta 
 * 
 * @author Edward Daniel Vidal García
 * @since Diciembre 2022
 */
@Entity
@Table(name = "CUENTA") 
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
@Data
public class Cuenta {	
		
	/** atributo para almacenar tipoCuenta */
	@Id
	@Column(name = "NUMERO_CUENTA")
	private Long numeroCuenta;
	
	/** atributo para almacenar tipoCuenta */
	@Column(name = "TIPO_CUENTA")
	private String tipoCuenta;
	
	/** atributo para almacenar saldo inicial */
	@Column(name = "SALDO_INICIAL")
	private Long saldoInicial;
	
	/** atributo para almacenar estado */
	@Column(name = "ESTADO")
	private String estado;
	
	/** Campo para guardar id del cliente */	
	@ManyToOne(optional = true ,fetch = FetchType.LAZY)
	private Cliente cliente;

}

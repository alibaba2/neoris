package com.neoris.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neoris.entitie.Cliente;
import com.neoris.repository.ClienteRepository;

@Service
public class ClienteService {
		
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Atributos de la clase
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** Objeto para realizar operaciones usando JPA con la entidad de cliente */
	@Autowired
	private ClienteRepository clienteRepository;

	
	/**
	 * Método que permite crear los clientes
	 * 
	 * @return
	 */
	public List<Cliente> createCliente(List<Cliente> data) {	
		List<Cliente> cliente = clienteRepository.saveAll(data);
		return cliente ;
	}

}

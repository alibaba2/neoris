package com.neoris.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neoris.dto.Filtros;
import com.neoris.entitie.Cuenta;
import com.neoris.entitie.Movimiento;
import com.neoris.repository.CuentaRepository;
import com.neoris.repository.CustomMovimientosRepository;
import com.neoris.repository.MovimientoRepository;

@Service
public class MovimientoService {

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Atributos de la clase
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** Objeto para realizar operaciones usando JPA con la entidad movimiento */
	@Autowired
	private MovimientoRepository movimientoRepository;
	
	/** Objeto para realizar operaciones usando JPA con la entidad cuenta */
	@Autowired
	private CuentaRepository cuentaRepository;
	
	@Autowired
	CustomMovimientosRepository customMovimientosRepository;
	
	
	
	/**
	 * Metodo para crear movimiento
	 * @param data
	 * @return
	 */
	public String createMovimiento(Movimiento data) {		
		Cuenta cuenta = cuentaRepository.findById(data.getCuenta().getNumeroCuenta()).get();		
		Long saldo = cuenta.getSaldoInicial();
		Long saldoFinal = saldo + data.getValor();
		LocalDate fecha = LocalDate.now();

		if(saldoFinal > 0) {						
			cuenta.setSaldoInicial(saldoFinal);
			cuentaRepository.save(cuenta);

			data.setFecha(fecha);
			data.setSaldo(saldoFinal);
			Movimiento movimiento = new Movimiento();
			movimiento.setCuenta(data.getCuenta());
			movimiento.setFecha(data.getFecha());
			movimiento.setSaldo(data.getSaldo());
			movimiento.setTipoMovimiento(data.getTipoMovimiento());
			movimiento.setValor(data.getValor());
			movimientoRepository.save(data);
			return "Done";
		}else {
			return "Saldo no disponible";
		}
	}
	
	public List<Movimiento> getMovimientos(Filtros filtro) {	
		System.out.println("desde = " + filtro.getDesde());
		System.out.println("hasta = " + filtro.getDesde());
		System.out.println("cliente = " + filtro.getCliente().getId());
		return customMovimientosRepository.getMovimientos(filtro);		  
	}
}

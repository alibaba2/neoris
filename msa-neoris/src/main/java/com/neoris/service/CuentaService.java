package com.neoris.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neoris.entitie.Cuenta;
import com.neoris.repository.CuentaRepository;

@Service
public class CuentaService {

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Atributos de la clase
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** Objeto para realizar operaciones usando JPA con la entidad cuenta */
	@Autowired
	private CuentaRepository cuentaRepository;
	
	
	/**
	* Método que permite crear las cuentas
	* 
	* @return
	*/
	public List<Cuenta> createCuenta(List<Cuenta> data) {	
		List<Cuenta> cuenta = cuentaRepository.saveAll(data);		
		return cuenta;		  
	}
		
}
